# Selected Problems in Computational Gravity #

This is the Ph.D. thesis in physics for Jonah Miller, to be submitted
to the University of Guelph. Prepared according to the guidelines
found
[here](https://www.uoguelph.ca/graduatestudies/current-students/preparation-your-thesis).

### Building ###

* This is a latex repo.
* With GNU make installed, simply type `make`

### Who do I talk to? ###

* Email Jonah Miller at [jonah.maxwell.miller@gmail.com](mailto:jonah.maxwell.miller@gmail.com)

### Acknowledgements ###

LaTeX template based on work of [Joniada Milla](http://www.jmilla.com/)
