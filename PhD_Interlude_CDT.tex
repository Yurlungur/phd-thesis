% \chapter{Interlude on Lattice Quantum Gravity}
% \label{chap:interlude:CDT}

Chapters \ref{chap:firstlook} and \ref{chap:secondlook} contain
\cite{JHC&JMM} and \cite{SecondLook} respectively, which are parts one
and two of a two-part series. In this series, my collaborators and I
studied the \textit{thick-sandwich problem} in causal dynamical
triangulations (CDT): Given the geometry of a spacelike hypersurface
at some initial time and the geometry of one at some final time, what
is the probability of the former transitioning into the latter? We
provide some preliminary answers for CDT. In the process, we developed a
formalism and the appropriate numerical tools to delve further into
these questions.

Taking the continuum limit of a lattice field theory is the converse
of the discretization procedure one undertakes when numerically
solving a differential equation. In the latter case, one takes the
continuum equation and finds a discrete system that well-approximates
it. In CDT, conversely, one instead takes a discrete model of quantum
general relativity, performs a numerical experiment, and then asks
what continuum theory matches the experiment.\footnote{The ultraviolet
  cutoff in CDT is not considered physical, but an artifact of the
  regularization procedure. Nevertheless, the discrete theory, not the
  continuum one, is the first-class citizen.} For sufficiently long
length and time scales (or sufficiently many discrete building
blocks), the theory should match classical GR---one of the early
triumphs of CDT was the demonstration that de Sitter spacetime
dominates the vacuum path integral (with positive cosmological
constant). However, between the classical limit and the ultraviolet
cutoff, an effective field theory should emerge which may or may not
be GR. Much of the research in CDT is aimed at understanding what this
effective theory is, and progress has been made in this direction. For
example, there is some evidence that the low-energy limit of CDT is
Ho\v{r}ava-Lifshitz gravity
\cite{JA&AG&SJ&JJ&RL,JA&LG&YS&YW,CA&SJC&JHC&PH&RKK&PZ}.

Effective field theories are not necessarily unique, however, and it
is often difficult to extract physical meaning from
them. \cite{JHC&JMM} and \cite{SecondLook} show one such example. In
\cite{JHC&JMM} (chapter \ref{chap:firstlook}), we performed numerical
experiments that are well-described by an effective theory with a
signature change in it---the physical spacetime transitions from
Lorentzian signature to Euclidean. In \cite{JHC&JMM}, we attributed
physical meaning to this effective action. However, in
\cite{SecondLook} (chapter \ref{chap:secondlook}), we show that a
simpler effective theory without signature change works just as
well. We believe the simpler theory is the one with physical meaning.


