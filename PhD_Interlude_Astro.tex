% \chapter{Interlude on Relativistic Astrophysics}
% \label{chap:interlude:astro}

Chapters \ref{chap:DGFE} and \ref{chap:KBHsPH} approach relativistic
astrophysics from two different perspectives. Chapter \ref{chap:DGFE}
is motivated by the need to numerically solve \textit{a broad class}
of problems in an efficient manner. It therefore develops a novel
tool---a discretization method---which has the potential to improve
the efficiency of calculations in the future. Chapter
\ref{chap:KBHsPH} contains the first steps necessary for solving
\textit{one specific} problem in relativistic astrophysics. That said,
both problems are united by the need to wrestle with the gauge freedom
of general relativity.

Although they completely describe the relevant physics, the Einstein
equations form an underdetermined system when translated into the
language of partial differential equations. To complete them, one must
choose a coordinate system---a gauge.\footnote{One may also choose a
  coordinate-free representation, such as the Regge calculus used in
  the previous chapters or a Tetrad-type formalism. The latter can be
  thought of as choosing a new gauge at each point on the
  manifold; the former, as approximating the Lie group of
  diffeomorphisms as a finite-sized discrete symmetry group.}  Decades
of hard experience has taught the relativity community that some gauge
choices are better than others and that a good gauge is adapted to the
problem one wishes to solve. Without a good choice of
coordinates---and a good way of describing them in the language of
partial differential equations---there is not even a guarantee that GR
forms a well-posed initial value problem.\footnote{Not all problems
  are best formulated as initial value problems, of course.}  (For a
review of this topic, see \cite{Sarbach2012}).

In \cite{OLDG} (chapter \ref{chap:DGFE}) I developed, with advice from
my supervisor, a novel numerical method which substantially reduces
memory and communication overheads for a given accuracy. We took
special care to make sure our method is compatible with the
formulations of the Einstein equations and the gauge choices popular
in numerical relativity.

Recall from chapters \ref{chap:firstlook} and \ref{chap:secondlook}
that in lattice quantum field theory, one starts with a discrete theory
and asks what continuum (low-energy) theory it is compatible with. In
\cite{OLDG}, we took the opposite stance. We started with a continuum
model and rigorously built a discrete approximation which is
guaranteed to be compatible with a particular formulation of the
Einstein equations. One way we ensure this compatibility is by
demanding that certain discrete operators retain the algebraic
properties of their continuum counterparts.

Sometimes a problem can be most easily solved by utilizing several
different coordinate systems. Such is the case with the problem
discussed in chapter \ref{chap:KBHsPH}. In \cite{KBsPH}, Herdeiro,
Radu, and R{\'u}narsson presented a set of stationary spacetimes
representing black holes with massive vector hair. They arrived at
these solutions by solving a time-independent problem, which is
formulated as an elliptic system of partial differential equations. To
do so, they chose a particularly simple, manifestly time-independent
coordinate system.

By utilizing numerical tools developed by William East, I am now
studying the time-dependent behaviour of perturbations to these hairy
black hole solutions. Unfortunately, Herdeiro, Radu, and
R{\'u}narsson's coordinate system is ill adapted to dynamical
simulations. As a necessary first step, I therefore present a new
coordinate system better adapted to this problem.
