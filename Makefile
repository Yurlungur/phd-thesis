TEX_MASTER = PhD_00_Thesis.tex
BIB = PhD_Bibliography.bib
FIG = horizon-penetrating.pdf
TEX_SRC=header.tex\
	uogthesis.cls\
	habbrv.bst\
	PhD_01_TitlePage.tex\
	PhD_02_Abstract.tex\
	PhD_03_Dedication.tex\
	PhD_04_Acknowledgements.tex\
	PhD_05_TOC.tex\
	PhD_Chapter_Intro.tex\
	PhD_Interlude_CDT.tex\
	PhD_Chapter_First_Look.tex\
	PhD_Chapter_Second_Look.tex\
	PhD_Interlude_Astro.tex\
	PhD_Chapter_DGFE.tex\
	PhD_Chapter_hair.tex\
	PhD_Chapter_Conclusion.tex\
	horizon-penetrating.tex

ALL_DEP = ${TEX_SRC} ${BIB} ${FIG}
ALL = ${TEX_MASTER} ${ALL_DEP}
POSTFIXES = aux log pdf lot out pdfsync toc blg lof bbl

all: PhD_00_Thesis.pdf

.PHONY: all clean

PhD_00_Thesis.pdf: PhD_00_Thesis.tex ${ALL_DEP} 
	latexmk -pdf -halt-on-error $<

horizon-penetrating.pdf: horizon-penetrating.tex
	pdflatex -halt-on-error $<

clean:
	$(foreach name,$(ALL),$(RM) $(addprefix $(basename $(name)).,$(POSTFIXES));)
	$(RM) com.aux

