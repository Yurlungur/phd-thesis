\chapter{Computational Gravity}
\label{chap:conclusion}

\epigraph{More is different.}{\textit{P. W. Anderson}
  \cite{anderson1972more}}

Over the last several hundred pages, we have explored three topics in
computational gravity and drawn connections between lattice quantum
gravity and numerical relativistic astrophysics. I now take a few
paragraphs to explore \textit{why} these topics are
connected. Obviously as the name implies, both topics require
computers, but there are deeper principles at work---after all, the
first numerical algorithms were performed with pen (or perhaps quill)
and paper, not a computer.\footnote{For example, see
  \cite{NewtonHistory} for a brief history on Newton's method for root
  finding. It was discovered earlier than Newton.}  Lattice quantum
gravity and numerical relativity have two important traits in common:
(1) the need to wrestle with the gauge freedom of general relativity
and (2) the care one must take when matching a discrete system to a
continuum one.

\section{Gauge Freedom}
\label{sec:conclusion:gauge}

Practitioners of both classical and quantum gravity must wrestle with
the gauge freedom of general relativity. Path integral formulations in
quantum gravity must wrestle with the ambiguity that emerges from this
gauge freedom. Classically, one must choose a gauge to work in and the
best gauges for initial value problems are different than the best
gauges for boundary value problems. Therefore, when one wishes to
connect these two classes of problem, one may need to perform a
coordinate transformation.

As alluded to in chapters \ref{chap:firstlook} and
\ref{chap:secondlook}, one major motivation for CDT is the fact that
the measure $d\mu(\mathbf{g})$ in the gravitational path integral
\begin{equation}
  \label{eq:conclusion:path:integral}
  \mathscr{A}[\gamma]=\int_{\mathbf{g}|_{\partial\mathcal{M}}=\gamma}\mathrm{d}\mu(\mathbf{g})\,e^{iS_{\mathrm{cl}}[\mathbf{g}]}
\end{equation}
is not known. The problem is the ambiguity of the metric
$\mathbf{g}$---many metrics correspond to the same physical
geometry. By replacing a continuum geometry with a piecewise flat one,
curvature can be encoded in the connectivity of a dual graph. In this
coordinate-free representation, the metric ambiguity is no longer
present. This ambiguity in the metric is, of course, nothing more than
the gauge freedom of general relativity. This discussion is not unique
to CDT. Other discrete theories of quantum gravity are motivated by
the same measure problem and resolve it by similar techniques. Indeed,
understanding and wrestling with this gauge freedom is one of the
major themes of quantum gravity research \cite{SorkinForks}.

Studies of relativistic astrophysics must also contend with this gauge
freedom, albeit not in the same way. To formulate the Einstein
equations as a well-posed initial value problem, it is necessary (but
not sufficient) to choose gauge conditions that ensure that, for
hyperbolic formulations, characteristic perturbations of the system
propagate at finite and bounded coordinate velocity
\cite{Sarbach2012}.\footnote{Of course, the Einstein equations do not
  \textit{need} to be formulated as a hyperbolic system. One may
  choose a gauge that makes them either elliptic or parabolic. In the
  former case, they become a \textit{boundary}-value problem, rather
  than an initial-value problem.}  Chapter \ref{chap:DGFE} is devoted
to developing a discretization scheme compatible with one particularly
successful formulation of the Einstein equations, the BSSN formulation
\cite{ShibataBSSN,BaumgarteBSSN,BrownBSSN,AlcubierreConformalDecomp,AlcubierreGammaDriver}.

Not all problems are best formulated as initial-value problems. The no
hair theorems assert that (under certain assumptions) all
\textit{stationary} black hole spacetimes are completely described by
three numbers: the mass, angular momentum, and charge of the black
hole. If one seeks to evade these theorems, one searches for
\textit{time-independent} solutions to the Einstein equations. This is
an elliptic boundary-value problem.

In \cite{KBsPH}, Herdeiro, Radu, and R{\'u}narsson solve this elliptic
problem for black holes with massive vector hair. However, to do so,
they choose a coordinate system ill-suited for initial-value
problems. I am in the process of simulating time-dependent
perturbations to Herdeiro, Radu, and R{\'u}narsson's hairy black
holes, which requires I first translate the solutions into a form
compatible with GR as an initial-value problem. Chapter
\ref{chap:KBHsPH} is devoted to this necessary first step.

\section{Discretuum and Continuum}
\label{sec:conclusion:matching}

When one discretely approximates a partial differential equation, it
is tempting to believe that only one choice has been made---one
discretization scheme. In fact, one has made at least two choices: how
to discretely approximate the differential equations, and how to
approximate their solution. These choices are not necessarily
compatible and this potential ambiguity is characteristic not only of
partial differential equations, but of any continuum system
approximated by a finite number of degrees of freedom.

Consider system
\begin{equation}
  \label{eq:conclusion:hyperbolic}
  \mathcal{R}^i[u^i,\partial_t u^i, \partial_j u^i, \partial_j \partial_k u^i] 
  = \partial_t u^i - \mathcal{L}^i[u^i,\partial_j u^i, \partial_j \partial_k u^i ]
  = 0 
\end{equation}
where $u^i$ is a collection of variables and $\mathcal{L}^i$ is a
nonlinear operator that acts on $u$ and its spacelike
gradients. Further suppose that $\mathcal{L}^i$ is constructed such
that, subject to appropriate initial and boundary conditions, equation
\eqref{eq:conclusion:hyperbolic} forms a well-posed initial value
problem. We wish to approximate equation
\eqref{eq:conclusion:hyperbolic} and its solutions $u^i$ with a
discrete, finite number of degrees of freedom $N$.

Three properties are necessary for a successful discrete approximation
of a well-posed initial-value problem in the continuum:
\textit{consistency}, \textit{convergence}, and \textit{stability}. A
discrete approximation $\bar{\mathcal{R}}^i$ is \textit{consistent}
with $\mathcal{R}^i$ if it converges to $\mathcal{R}^i$ as
$N\to\infty$. A discrete approximation $\bar{R}^i$ is
\textit{convergent} if the discrete solutions $\bar{u}^i$ of
$\bar{\mathcal{R}}^i$ converge to the solutions $u^i$ of
$\mathcal{R}^i$ as $N\to\infty$. A discrete approximation
$\bar{\mathcal{R}}^i$ is \textit{stable} if the infinity
norm\footnote{Or any appropriate norm.} $|\bar{u}|_\infty$ of discrete
solutions $\bar{u}^i$ grows no more quickly than exponentially in
time, with a rate that is bounded independent of the number of degrees
of freedom $N$. More simply: \textit{consistency} is the agreement of
the discrete approximation of the differential operator with the
continuum system, \textit{convergence} is the agreement of the
discrete solutions to the initial-value problem with the continuum
solutions, and \textit{stability} is the well-posedness of the
discrete initial value problem. In their landmark theorem, Lax and
Richtmyer demonstrated that a finite-differences approximation of
equation \eqref{eq:conclusion:hyperbolic} system is convergent if and
only if it is both consistent and stable. In fact, if a system
possesses any two of these properties, it is guaranteed to possess the
third.

In fact, a finite differences
discretization possesses one of these three properties if 
it also possesses the other two.

The theorem I just described is only applicable to hyperbolic
differential equations. However, it hints at a deeper truth: it is
possible to choose a discrete approximation of a \textit{problem} and
a discrete approximation to \textit{solutions} of that problem which
are incompatible. There is, in other words, a \textit{degeneracy of
  discretizations.} In chapter \ref{chap:DGFE}, the aforementioned
theory is directly applicable. My collaborator and I developed a new
discretization scheme, and we were careful to ensure that our
discretizations of general relativity were consistent, convergent, and
stable. However, I argue that Lax-Richtmyer theory is also indirectly
applicable to the discussions of CDT in chapters \ref{chap:firstlook}
and \ref{chap:secondlook}. In particular, the distinction between
consistency and convergence provides a useful lens through which to
view lattice quantum gravity.

The CDT path sum
\begin{equation}
  \label{eq:conclusion:path:sum}
  \mathcal{A}[\Gamma]=\sum_{\substack{\mathcal{T}_{c} \\
      \mathcal{T}_{c}|_{\partial\mathcal{T}_{c}}=\Gamma}}\mu(\mathcal{T}_{c})e^{iS_{R}[\mathcal{T}_{c}]}
\end{equation}
is a \textit{consistent} approximation of the path integral
\eqref{eq:conclusion:path:integral}
\begin{displaymath}
  \mathscr{A}[\gamma]=\int_{\mathbf{g}|_{\partial\mathcal{M}}=\gamma}\mathrm{d}\mu(\mathbf{g})\,e^{iS_{\mathrm{cl}}[\mathbf{g}]}.
\end{displaymath}
However, it is not clear that the triangulations $\mathcal{T}_{c}$ that
dominate the path sum \eqref{eq:conclusion:path:sum} converge to the
spacetimes that dominate the path integral
\eqref{eq:conclusion:path:integral}. To ensure that Wick rotation is
well defined in CDT, we demand that our triangulations possess a
foliation of spacelike hypersurfaces and that these spacelike
hypersurfaces must all possess the same topology. It is not clear
whether these restrictions are justified---i.e., that all physically
meaningful quantum spacetimes will possess these
properties.\footnote{This is different from the gauge a ambiguity
  discussed in section \ref{sec:conclusion:gauge}. Rather it is a
  technical condition of CDT. Indeed, there is some evidence that the
  foliation condition can be relaxed \cite{CDTNoFoliation}.} In vacuum
and with positive cosmological constant, the dominant triangulations
in the path sum converge to de Sitter space. This was one of the major
early triumphs of CDT, indicating it may have the appropriate
classical limit. However, this success is far from a formal proof. It
is not known if the low-energy limit of CDT matches general relativity
in more general settings.

The search for effective field theories that are well approximated by
CDT can also be cast into this language. We know that the
triangulations that dominate the CDT path sum converge to the
solutions of \textit{some} effective system in the continuum, but we
don't know what it is. In other words, we are searching for a
continuum system with which our discrete system is
\textit{consistent}.

% \vspace{1cm}
\section{Conclusion}
\label{sec:conclusion:computational:gravity}

The deep connection between field theory (both classical and quantum)
and statistical physics has long been understood. If one has a
continuum of bodies, then many-body physics becomes field theory. The
much-utilized converse relationship is rarely remarked upon, however:
the countable approximation of a field theory is a many-body
problem. It is this converse relationship that one utilizes when one
regularizes a field theory with an ultraviolet cutoff or approximates
a PDE system numerically. In this work, I have explored this converse
relationship through three topics in computational gravity: causal
dynamical triangulations, operator-based discontinuous Galerkin
methods, and black holes with massive vector hair.
