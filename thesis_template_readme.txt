Thesis template by Joniada Milla (jmilla.com)
Designed to match with 2013 guidelines at the university of Guelph.

PhD_00_Thesis.tex is the master file and calls in all the other
complementary files to compile them in a single document.

The package file uogthesis.cls should be kept in the same folder as
the rest of the files.

Congratulations on being close to completion or having already
defended your thesis!

