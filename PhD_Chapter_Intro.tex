\chapter{Introduction}
\label{chap:intro}

\epigraph{Using a term like nonlinear science is like referring to the
  bulk of zoology as the study of non-elephant
  animals.}{\textit{Stanis\l{}aw Ulam}}

This work discusses three topics united by a need to solve problems
in gravity and general relativity using computational techniques.
Discrete quantum gravity and numerical relativistic astrophysics share
many surprising connections. Although I do not explore these
connections in detail when I attack these problems, these connections
will appear if one steps back from the trees and observes the
forest. I therefore propose a new name for the study of topics at the
intersection of numerical relativity and quantum gravity:
computational gravity.

\section{The Paths to Computational Gravity}
\label{intro:paths}

Einstein's theory of general relativity (GR)
\cite{Einstein1,Einstein2} elegantly combines space, time, and
geometry to describe gravitational systems. GR has been wildly
successful. It has passed an array of weak-field tests
\cite{WillTestGR}, its regime of validity ranges from compact object
astrophysics \cite{shapiro2008black} to cosmology
\cite{wald2010general},\footnote{Although GR has been very successful
  in a cosmological context, cosmology is arguably the field where
  problems with GR are readily visible in the presence of the unknown
  dark sector.} and the recent direct detection of gravitational waves
predicted by GR \cite{LIGODetection,GW151226} have opened the door to
gravitational wave astronomy.

When unpacked into the language of partial differential equations, the
conceptually clean language of the geometry of curved manifolds
becomes complex and non-linear. Since Einstein first presented his
theory 102 years ago, exact \cite{PoissonToolkit,stephani2009exact}
and perturbative \cite{poisson2014gravity} solutions to this complex
nonlinear system have proliferated and GR has thrived, both as a tool
for astrophysics and cosmology and as an independent field. The field
has now matured to the point where its practitioners routinely tackle
complex, nonlinear problems both analytically and numerically.

% As with almost any complex, nonlinear system, however, sometimes one
% must resort to numerical calculations.

Numerical relativity is the field of study where one solves problems
in classical general relativity using numerical techniques. In
numerical relativity (and most numerical approaches to PDE theory),
the Einstein equations and their solutions are approximated by a
discrete, finite number of degrees of freedom. Numerical simulations
were critical in the recent direct detection and analysis of
gravitational waves \cite{LIGODetection,GW151226} and they will likely
play an important role in relativistic astrophysics for the
foreseeable future.

Research in quantum gravity arrived at numerical calculations from a
rather different perspective. Naive quantizations of the Einstein
field equations suffer from an ultraviolet divergence that does not
appear amenable to power-counting renormalization
techniques\footnote{There are promising approaches to resolving this
  divergence by searching for fixed-points in the renormalization
  group flow. For a review, see \cite{AsymptoticSafetyLRR}.} and the
correct measure in path integral formulations is not known. One can
cure both of these ills by introducing an ultraviolet cutoff and
\textit{discretizing} the geometry.
% It is a common claim that GR is non-renormalizable. However, this is
% not actually known. For example,

Depending on the approach, the discrete theory may be physical or it
may simply be an approximation technique. In the case of the latter,
the connection to numerical relativity is clear: both approaches use a
system with a finite number of degrees of freedom to approximate one
with an infinite number. In either case, the combinatorics of this
discrete geometry usually necessitate numerical simulations. In this
work, I will discuss only one type of discrete quantum
gravity---causal dynamical triangulations---descended from a
particular lineage of approaches. However, I urge the reader to
investigate other promising approaches such as spin foams and causal
sets. See \cite{PerezFoams} and \cite{Sorkin2005} for respective
reviews.

% The naive ``sum over
% histories'' path integral for GR is conceptually\footnote{we have set
%   $\hbar=1$.}
% \begin{equation}
%   \label{eq:intro:path:integral:conceptual}
%   \mathcal{A}(\partial \mathcal{M}_0) = \int_{\mathcal{M}|\partial\mathcal{M}=\partial\mathcal{M}_0} e^{i S[\mathcal{M}]} \mathcal{D}[\mathcal{M}],
% \end{equation}
% i.e., the transition amplitude (also called the propogator) of a
% quantum spacetime with boundary geometry $\partial\mathcal{M}_0$ is
% given by the integral of the appropriate phase over all possible
% spacetimes $\mathcal{M}$ with this boundary metric. 
% 
% However, defining this path integral in a more rigorous and workable
% way is major challenge. The naive approach would be to use metrics to
% represent the geometry. If one takes this path, one arrives at the
% integral
% \begin{equation}
%   \label{eq:intro:path:integral:metric}
%   \mathcal{A}(\gamma) = \int_{\mathcal{M}|\partial\mathcal{M}=\gamma} e^{i S[g]} \mathcal{D}[g],
% \end{equation}
% or the transition amplitude of a spacetime with a boundary endowed
% with spacelike metric $\gamma$ is given by the path integral over all
% spacetime metrics $g$ that restrict to $\gamma$ on the
% boundary. Unfortunately, many metrics $g$ represent the same physical
% spacetime $\mathcal{M}$. And so the correct \textit{measure}
% $\mathcal{D}[g]$ is not known. 

% Integrals \eqref{eq:intro:path:integral:conceptual} and
% \eqref{eq:intro:path:integral:metric} (and indeed most approaches to
% quantum gravity) also suffer an ultraviolet divergence that does not
% appear amenable to naive power-counting renormalization
% techniques.\footnote{There are promising approaches to resolving this
%   divergence via more sophisticated renormalization techniques. For a
%   review, see \cite{AsymptoticSafetyLRR}.} How closely related this
% difficulty is to the measure problem is not known. 
% However, this is an
% approach that cures both difficulties: introduce an ultraviolet cutoff
% by \textit{discretizing} the geometry. The combinatorics of this
% discrete geometry usually necessitate numerical simulations. In this
% work, I will discuss only one type of discrete quantum gravity,
% descended from a particular lineage of approaches. However, I urge the
% reader to investigate other promising approaches such as spin foams
% and causal sets. See \cite{PerezFoams} and \cite{Sorkin2005} for
% respective reviews.

As we shall see, there are a surprising number of parallels between
numerical approaches to classical GR and to discrete quantum
gravity. For example, both numerical approximations of GR and quantum
theories of gravity must converge to the classical theory in the
appropriate limit. The former must do so as a necessary (but not
sufficient) condition for the validity of the approximation, while the
latter must match observations at low-energies. I urge the reader to
keep these connections in mind as they peruse the selected topics
below.

\section{A Brief Summary of Topics}
\label{intro:sec:summary}

In this work, I present two related studies of lattice quantum
gravity, one novel numerical method motivated by the needs of the
relativistic astrophysics community, and a study of dynamics in an
astrophysical context. The first two problems are presented as three
peer-review papers collected here. The last is a work in progress and
is not yet peer-reviewed.

Causal dynamical triangulations (CDT) is a sum-over-histories approach
to quantum gravity motivated by the success of lattice field theory
\cite{Ambjorn2012127}. In CDT, spacetime itself becomes the
lattice. Spacetimes are approximated, and thus regularized, as
piecewise flat manifolds constructed out of pieces of Minkowski
space. In \cite{JHC&JMM} (chapter \ref{chap:firstlook}), my
collaborator Josh Cooperman and I presented the first-ever studies of
transition amplitudes in CDT. We also uncovered a tantalizing
connection to the arrow of time, and evidence that the signature of
the metric in CDT may not be fixed. In \cite{SecondLook} (chapter
\ref{chap:secondlook}), along with Kyle Lee, we re-examined this
evidence and presented a simpler explanation. We believe that our case
study in effective signature change in CDT can shine light on other
speculative discussions of this topic such as those presented in
\cite{DNC&JJ} and \cite{JA&DNC&JGS&JJ}.

One difficulty in utilizing simulations to make predictions is that,
for fully (3+1)-dimensional general relativity, these simulations
require a very long runtime. Discontinuous Galerkin finite element
(DGFE) methods offer a faster alternative to the traditional
algorithms \cite{hesthaven2007nodal}. In collaboration with Erik
Schnetter, I constructed a generalization of DGFE methods compatible
with a broad class of systems including the BSSN formulation of the
Einstein equations. This work is presented in \cite{OLDG} and in
chapter \ref{chap:DGFE}.

A minimal coupling of massive vector fields---known as \textit{Proca}
fields \cite{proca1938theorie}---to gravity not only provides a toy
model with which to explore dynamics of modified
\cite{EinsteinAether,HoravaLifshitz} and unmodified gravity
\cite{ProcaKerrPerturbations}, but may be astrophysically relevant in
the context of stringy models of dark matter
\cite{MinaStringAxiverse}. Recently Herdeiro, Radu, and Runarsson
discovered a family of stationary black holes with Proca hair, evading
the no hair theorems \cite{KBsPH}. I am numerically studying the
nonlinear stability of these solutions to perturbation. In chapter
\ref{chap:KBHsPH}, I present the first steps required for such a
study. This study is a collaboration with William East.

%--------------------------------------------------------- Sec
%\section*{Appendix}
%\label{sec:gauge:appendix}
%\addcontentsline{toc}{section}{Appendices}

